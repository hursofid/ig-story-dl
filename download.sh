#!/bin/bash
set -e
# note that BSD sed used
# add question with conditions where to save concatenated media (set ~/Movies by default and if does not exist ask to specify path to save)
#usage: $0 [story]
if [[ ! $1 ]]
  then
    echo "Usage: $0 [ path to har ]"
    exit 1
fi
date=$(date "+%Y%m%d%H%M%S")
storypath=$1
storyname=$(basename "$1")
storydir=~/tmp/.$date-$storyname
counter=1

# setting videos/movies folder linux/macos
if [ -d ~/Videos ]; then
	final_output_location="~/Videos"
fi
if [ -d ~/Movies ]; then
	final_output_location="~/Movies"
fi
if [[ -z "$final_output_location" ]]; then
	echo "Neither ~/Videos nor ~/Movies were found on your system, where you want to save output?"
	read -p "Enter full path to new location: " final_output_location
fi

mkdir -p "$storydir"

# get rid of everything except media URLs
####### this throws exitcode 0 after pipe
grep -A 1 -i '"method": "GET"' "$storypath" | grep -v GET | grep ".mp4" | sed 's/^ *"url": "//;s/",//' > "$storydir"/urls

# get all media
#while read -r "$storydir"/urls
cat "$storydir"/urls | while read -r line
#for i in $(cat "$storydir"/urls)
  do
    wget -nv -t 1 -T 4 "$line" -O "$storydir"/IMG_"$(printf %04d "$counter")".mp4
    let counter=$((counter+01))
done

# create media list to supply ffmpeg with
find "$storydir" -type f -name '*.mp4' > "$storydir"/media.txt

# make media list compatible to import with ffmpeg
sed --in-place=.bak "s/^/file '/;s/$/'/" "$storydir"/media.txt

# concatenate media
ffmpeg -hide_banner -f concat -safe 0 -i "$storydir"/media.txt -c copy "$storydir"/"$storyname"-"$date".mp4

rm "$storydir"/urls
rm "$storydir"/media.txt
rm "$storydir"/media.txt.bak
find "$storydir" -type f -name 'IMG*' -exec rm -v {} \;
mv "$storydir"/"$storyname"-"$date".mp4 "$final_output_location"
rmdir "$storydir"
exit 0
