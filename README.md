Instagram stories download tool
=============

Pre-requisites
-------------

- ffmpeg
- wget
- bsd sed

Supported browsers
-------------

- Safari

Other browsers compatibility not tested (yet)


Usage:
-------------

1. Open https://www.instagram.com with Safari

2. Open developer tools with <kbd>⌘</kbd> + <kbd>⌥</kbd> + <kbd>I</kbd> and navigate to Network tab

3. Before opening desired story to capture, clear network items with <kbd>⌘</kbd> + <kbd>K</kbd>

4. Watch stories you wish to get downloaded and concatenated into single videofile (you don't need to watch them fully, just click next arrow button on webpage)

5. Filter out media by mp4 in search field

6. Once you've done, click on export button

7. Save har file on the disk and pass it's location to download.sh script as parameter

8. Run `download.sh stories.har` 

9. Find the result in `~/tmp` folder

Notes:
-------------

Unfortunately, recent changes on instagram side made static images being transferred as .jpg and they're not included anymore in this script.

Pull requests are welcome!
